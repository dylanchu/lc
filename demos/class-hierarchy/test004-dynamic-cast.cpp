#include<iostream>
using namespace std;

struct Base {virtual ~Base() = default;};

struct Derived : Base {};



int main () {
    cout << __FILE__ << endl;

    Derived d, *pd;


    try {
        cout << "11111111111 ---------------" << endl;
        Base *pb = &d;

        Base *p0 = &(dynamic_cast<Derived&>(*pb));  // 没问题,正常定位vtable取到RTTI
        cout << p0 << endl;

        Base *p = &(dynamic_cast<Derived&>(*(pb+1)));  // no RTTI data
        cout << p << endl;

    } catch (const std::exception& e) {
        cout << e.what() << endl;
    }

    try {
        cout << "22222222222 ---------------" << endl;
        auto s = reinterpret_cast<Base*>(0);
        Base *p = &(dynamic_cast<Derived&>(*s));  // 没问题
        cout << p << endl;
    } catch (const std::exception& e) {
        cout << e.what() << endl;
    }

    try {
        cout << "33333333333 ---------------" << endl;
        auto s = reinterpret_cast<Base*>(0xFFFFFFF);
        Base *p = &(dynamic_cast<Derived&>(*s));  // no RTTI data
        cout << p << endl;
    } catch (const std::exception& e) {
        cout << e.what() << endl;
    }


    try {
        cout << "44444444444 ---------------" << endl;
        auto s = (Base*)(0xFFFFFFF);
        Base *p = &(dynamic_cast<Derived&>(*s));  // no RTTI data
        cout << p << endl;
    } catch (const std::exception& e) {
        cout << e.what() << endl;
    }




}
