/*
 * https://stackoverflow.com/a/5924594/11336250
 */

#include <iostream>
#include <vector>

template <typename C, typename T, T (C::*getter)(), void (C::*setter)(const T&)>
struct Property
{
    C* instance;

    Property(C* instance) : instance(instance)
    {}

    operator T() const
    {
        return (instance->*getter)();
    }

    Property& operator=(const T& value)
    {
        (instance->*setter)(value);
        return *this;
    }

    template <typename C2, typename T2, T2 (C2::*getter2)(), void (C2::*setter2)(const T2&)>
    Property& operator=(const Property<C2, T2, getter2, setter2>& other)
    {
        return *this = (other.instance->*getter2)();
    }

    Property& operator=(const Property& other)
    {
        return *this = (other.instance->*getter)();
    }
};

//////////////////////////////////////////////////////////////////////////

struct Foo
{
    int x_, y_;

    void setX(const int& x)
    {
        x_ = x;
        std::cout << "x new value is " << x << "\n";
    }
    int getX()
    {
        std::cout << "reading x_\n";
        return x_;
    }

    void setY(const int& y)
    {
        y_ = y;
        std::cout << "y new value is " << y << "\n";
    }
    int getY()
    {
        std::cout << "reading y_\n";
        return y_;
    }

    Property<Foo, int, &Foo::getX, &Foo::setX> x;
    Property<Foo, int, &Foo::getY, &Foo::setY> y;

    Foo(int x0, int y0) : x_(x0), y_(y0), x(this), y(this)
    {}
};

int square(int x)
{
    return x * x;
}

int main(int argc, const char* argv[])
{
    Foo foo(10, 20);
    Foo foo2(100, 200);
    std::vector<int> v;
    for (auto i = 0u; i < 100u; ++i) {
        v.emplace_back(i);
    }
    std::cout << "--------------\n";
    std::cout << "v[foo.x]==" << v[foo.x] << std::endl;
    std::cout << "--------------\n";
    int arr[100];
    for (auto i = 0u; i < 100u; ++i) {
        arr[i] = i*10;
    }
    std::cout << "arr[foo.x]==" << arr[foo.x] << std::endl;
    std::cout << "arr[foo.y]==" << arr[foo.y] << std::endl;
    std::cout << "--------------\n";
    int x = foo.x;
    std::cout << x << "\n";
    int y = foo.y;
    std::cout << y << "\n";
    foo.x = 42;
    std::cout << "assigned!\n";
    x = foo.x;
    std::cout << x << "\n";
    std::cout << "same instance prop/prop assign!\n";
    foo.x = foo.y;
    std::cout << "different instances prop/prop assign\n";
    foo.x = foo2.x;
    std::cout << "calling a function accepting an int parameter\n";
    std::cout << "square(" << foo.x << ") = " << square(foo.x) << "\n";
    return 0;
}
