// g++ -fdump-lang-class your_file.cpp

class Base {
    int a;
    int b;
    int c;
    virtual void f(){};
};

class Base222 {
    int x;
    int y;
};


class Derived01 : public Base {
    void f() override {};
};

class DDerived001 : public Derived01 {
    void f() override {};
};


class MDerived : public Base, public Base222 {};

class H1 : virtual public Base {
    int x;
    int y;
    int z;
};
class H2 : virtual public Base {
    int y;
};
class VD : public H1, public H2 {
    void f(){};

};



int main () {
}
