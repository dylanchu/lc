#include <stdio.h>
#include "entt/entity/entity.hpp"
#include "entt/entt.hpp"
#include "fmt/printf.h"

struct PositionComp {
    float x,y;
};

struct VelocityComp {
    float x, y;
};

void update(entt::registry &registry) {
  registry.view<PositionComp, VelocityComp>().each(
      [](PositionComp &pc, VelocityComp &vc) {
        pc.x += vc.x;
        pc.y += vc.y;
    });
}

int main() {
    entt::registry registry;
    for (auto i = 0u; i < 10u; ++i) {
      const auto ett = registry.create();
      registry.emplace<PositionComp>(ett, i*.1f, i*.1f);
      if (i % 2) {
        registry.emplace<VelocityComp>(ett, i*.1f, i*.1f);
      }
    }

    puts("---------------");
    registry.view<PositionComp>().each([](entt::entity ett, PositionComp &pc) {
        fmt::printf("%d: x=%.2f, y=%.2f\n", entt::to_integral(ett), pc.x, pc.y);
    });

    update(registry);
    update(registry);
    update(registry);

    puts("---------------");
    registry.view<PositionComp>().each([](entt::entity ett, PositionComp &pc) {
        fmt::printf("%d: x=%.2f, y=%.2f\n", entt::to_integral(ett), pc.x, pc.y);
    });

    return 0;
}