#include <iostream>
#include <random>
#include <vector>

/**
 * @brief 时间复杂度O(1)的权重随机池
 * https://blog.bruce-hill.com/a-faster-weighted-random-choice
 */
class WeightedRandomPool {
public:
  std::vector<double> weights_;
  std::vector<std::pair<double, int>> aliases_;

  WeightedRandomPool(const std::vector<double> &weights);
  void setup(std::vector<double> weights) noexcept;
  int random() noexcept;
};

inline void WeightedRandomPool::setup(std::vector<double> weights) noexcept {
  weights_ = std::move(weights);
  double sum = 0;
  for (auto w : weights_) {
    sum += w;
  }
  auto avg = sum / weights_.size();

  aliases_.resize(weights_.size(), {1, -1});
  std::vector<std::pair<int, double>> smalls;
  std::vector<std::pair<int, double>> bigs;

  for (auto i = 0; i < weights_.size(); ++i) {
    auto w = weights_[i] / avg;
    if (w < 1) {
      smalls.emplace_back(i, w);
    } else {
      bigs.emplace_back(i, w);
    }
  }

  auto it_small = smalls.begin();
  auto it_big = bigs.begin();
  while (it_big != bigs.end() && it_small != smalls.end()) {
    aliases_[it_small->first] = {it_small->second, it_big->first};
    it_big->second -= 1 - it_small->second;
    if (it_big->second < 1) {
      *it_small = *it_big;
      ++it_big;
    } else {
      ++it_small;
    }
  }
}

inline WeightedRandomPool::WeightedRandomPool(
    const std::vector<double> &weights) {
  setup(weights);
}

/**
 * Randomly select 1 element index with configured weights.
 * @return index of the random selected, might be -1 if invalid
 */
inline int WeightedRandomPool::random() noexcept {
  if (aliases_.empty())
    return -1;

  // TODO: use fixmath random
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dis(0, weights_.size());

  auto r = dis(gen);
  // auto i = r.to_int();
  auto i = std::floor(r);
  auto odds = aliases_[i].first;
  auto alias = aliases_[i].second;
  return (r - i) > odds ? alias : i;
}

int main() {
  auto pool = WeightedRandomPool({0.1, 0.5, 0.8, 0.3});

  std::vector<int> statistics = {0, 0, 0, 0};
  for (auto i = 0; i < 170000; ++i) {
    // std::cout << pool.random() << std::endl;
    ++statistics[pool.random()];
  }

  for (auto x : statistics) {
    std::cout << x << ',';
  }
  std::cout << std::endl;

  return 0;
}