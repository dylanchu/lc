#include <thread>
#include <chrono>
#include <Windows.h>

// 10ms为单位,大概一次系统调度的时间
void occupy_cpu1() {
    for (;;) {
        for (int i=0; i < 15600000; ++i);
        std::this_thread::sleep_for(std::chrono::milliseconds(10)); // <Windows.h> Sleep(10);
    }
}

void occupy_cpu2() {
    const auto busyTime = 10u;
    const auto idleTime = 10u;
    for (;;) {
        auto startTime = GetTickCount();
        while(GetTickCount() - startTime <= busyTime);
        Sleep(idleTime);
    }
}

int main() {
    SetThreadAffinityMask(GetCurrentThread(),1);
    occupy_cpu2();
    return 0;
}