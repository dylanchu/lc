/*
 * @lc app=leetcode.cn id=560 lang=cpp
 *
 * [560] 和为 K 的子数组
 *
 * https://leetcode.cn/problems/subarray-sum-equals-k/description/
 *
 * algorithms
 * Medium (44.09%)
 * Likes:    2360
 * Dislikes: 0
 * Total Accepted:    467.2K
 * Total Submissions: 1.1M
 * Testcase Example:  '[1,1,1]\n2'
 *
 * 给你一个整数数组 nums 和一个整数 k ，请你统计并返回 该数组中和为 k 的子数组的个数 。
 * 
 * 子数组是数组中元素的连续非空序列。
 * 
 * 
 * 
 * 示例 1：
 * 
 * 
 * 输入：nums = [1,1,1], k = 2
 * 输出：2
 * 
 * 
 * 示例 2：
 * 
 * 
 * 输入：nums = [1,2,3], k = 3
 * 输出：2
 * 
 * 
 * 
 * 
 * 提示：
 * 
 * 
 * 1 <= nums.length <= 2 * 10^4
 * -1000 <= nums[i] <= 1000
 * -10^7 <= k <= 10^7
 * 
 * 
 */
#include "utils.hpp"

// @lc code=start
class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        // 前缀和，非常经典的题！！
        // https://www.bilibili.com/video/BV1d54y127ri/

        int ret=0;
        unordered_map<int, int> m;
        m[0] = 1;

        int sum=0;
        for (int i=0; i<nums.size(); ++i) {
            sum += nums[i];
            if (m.count(sum - k)) {  // 意味着  前i个数的和 - 前x个数的和 = k，即 [某x,i] 是一个满足条件的子数组
                ret += m[sum - k];
            }
            m[sum]++;
        }
        return ret;
    }
};
// @lc code=end

