/*
 * @lc app=leetcode.cn id=198 lang=cpp
 *
 * [198] 打家劫舍
 */

// @lc code=start
class Solution {
public:
    int rob(vector<int>& nums) {
        // 状态转移方程（小到大）:
        // dp[i] = max(0 + dp[i-1], nums[i] + dp[i-2])  , 不是 dp[i] = max(nums[i] + dp[i+2], nums[i+1] + dp[i+3])
        // 临界情况:
        // dp[0]=nums[0]
        // dp[1]=max(nums[0], nums[1])
        std::vector<int> dp(nums.size());
        if (nums.size() == 0) return 0;
        if (nums.size() == 1) return nums[0];
        dp[0] = nums[0];
        dp[1] = std::max(nums[0], nums[1]);
        for (int i=2; i<nums.size(); ++i) {
            dp[i] = std::max(dp[i-1], nums[i] + dp[i-2]);
        }
        return dp[nums.size() - 1];
    }
};
// @lc code=end

