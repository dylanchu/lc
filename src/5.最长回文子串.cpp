/*
 * @lc app=leetcode.cn id=5 lang=cpp
 *
 * [5] 最长回文子串
 */

// @lc code=start
class Solution {
public:
    string longestPalindrome(string s) {
        // dp[i][j] 表示 s[i~j] 是否为回文子串
        bool dp[s.size()][s.size()];
        int reti=0, retj=0;
        // dp[i][j] = true;                i==j
        // dp[i][j] = s[i]==s[j];          j-i == 1  AA  AB
        // dp[i][j] = s[i]==s[j] && dp[i+1][j-1];      j-i >= 2   # i+1=>i,j-1=>j  循环迭代时i倒序,j正序

        for (int i=s.size()-1; i>=0; --i) {
            for (int j=i; j<s.size(); ++j) {
                if (i == j) {
                    dp[i][j] = true;
                }
                else if (j-i == 1) {
                    dp[i][j] = s[i]==s[j];
                }
                else {  // j-i >= 2
                    dp[i][j] = s[i]==s[j] && dp[i+1][j-1];
                }
                // 若s[i~j]是回文串则尝试更新结果
                if (dp[i][j] && j-i > retj-reti) {
                    retj = j;
                    reti = i;
                }
            }
        }
        return s.substr(reti,retj-reti+1);
    }
};
// @lc code=end

