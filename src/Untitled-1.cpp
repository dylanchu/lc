#include<stdio.h>

int (*func(int i))[3] {
    int arr[3]{i, i+1, i+2};
    return &arr;
}


int main() {
    int* arr = *func(8);
    for (int i=0; i<3; i++) {
        printf("%d\n", arr[i]);
    }
    return 0;
}