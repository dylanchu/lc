/*
 * @lc app=leetcode.cn id=1673 lang=cpp
 *
 * [1673] 找出最具竞争力的子序列
 *
 * https://leetcode.cn/problems/find-the-most-competitive-subsequence/description/
 *
 * algorithms
 * Medium (40.92%)
 * Likes:    153
 * Dislikes: 0
 * Total Accepted:    20.9K
 * Total Submissions: 45.1K
 * Testcase Example:  '[3,5,2,6]\n2'
 *
 * 给你一个整数数组 nums 和一个正整数 k ，返回长度为 k 且最具 竞争力 的 nums 子序列。
 * 
 * 数组的子序列是从数组中删除一些元素（可能不删除元素）得到的序列。
 * 
 * 在子序列 a 和子序列 b 第一个不相同的位置上，如果 a 中的数字小于 b 中对应的数字，那么我们称子序列 a 比子序列 b（相同长度下）更具 竞争力
 * 。 例如，[1,3,4] 比 [1,3,5] 更具竞争力，在第一个不相同的位置，也就是最后一个位置上， 4 小于 5 。
 * 
 * 
 * 
 * 示例 1：
 * 
 * 
 * 输入：nums = [3,5,2,6], k = 2
 * 输出：[2,6]
 * 解释：在所有可能的子序列集合 {[3,5], [3,2], [3,6], [5,2], [5,6], [2,6]} 中，[2,6] 最具竞争力。
 * 
 * 
 * 示例 2：
 * 
 * 
 * 输入：nums = [2,4,3,3,5,4,9,6], k = 4
 * 输出：[2,3,3,4]
 * 
 * 
 * 
 * 
 * 提示：
 * 
 * 
 * 1 
 * 0 
 * 1 
 * 
 * 
 */
#include "utils.hpp"

// @lc code=start
class Solution {
public:
    vector<int> mostCompetitive(vector<int>& nums, int k) {
        // 单调栈
        vector<int> st;
        st.reserve(k);
        int total_cnt = nums.size();
        for (int i=0; i<total_cnt; ++i) {
            // 如果要用 st.size() > x 比较,务必注意x不可为负数!!
            while (!st.empty() && (st.size() + (total_cnt-i) > k) && nums[i] < st.back()) {
                st.pop_back();
            }
            if (st.size() < k)
                st.push_back(nums[i]);
        }
        return st;
    }
};
// @lc code=end

// 这题卡在 (st.size() > keep_size) 了，keep_size为负数的时候结果居然是false!!!


int main() {
    std::cout << ((unsigned)10 > (int)-2) << std::endl;         // false
    std::cout << ((int)(unsigned)10 > (int)-2) << std::endl;    // true
    std::cout << ((int)-2 < (int)(unsigned)10) << std::endl;    // true
    Solution so;
    vector<int> v = {2,4,3,3, 5, 4, 9, 6};
    utils::print_seq(so.mostCompetitive(v, 4));
    return 0;
}