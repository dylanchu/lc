/*
 * @lc app=leetcode.cn id=437 lang=cpp
 *
 * [437] 路径总和 III
 *
 * https://leetcode.cn/problems/path-sum-iii/description/
 *
 * algorithms
 * Medium (47.74%)
 * Likes:    1856
 * Dislikes: 0
 * Total Accepted:    304.7K
 * Total Submissions: 638.3K
 * Testcase Example:  '[10,5,-3,3,2,null,11,3,-2,null,1]\n8'
 *
 * 给定一个二叉树的根节点 root ，和一个整数 targetSum ，求该二叉树里节点值之和等于 targetSum 的 路径 的数目。
 * 
 * 路径 不需要从根节点开始，也不需要在叶子节点结束，但是路径方向必须是向下的（只能从父节点到子节点）。
 * 
 * 
 * 
 * 示例 1：
 * 
 * 
 * 
 * 
 * 输入：root = [10,5,-3,3,2,null,11,3,-2,null,1], targetSum = 8
 * 输出：3
 * 解释：和等于 8 的路径有 3 条，如图所示。
 * 
 * 
 * 示例 2：
 * 
 * 
 * 输入：root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
 * 输出：3
 * 
 * 
 * 
 * 
 * 提示:
 * 
 * 
 * 二叉树的节点个数的范围是 [0,1000]
 * -10^9  
 * -1000  
 * 
 * 
 */
#include "utils.hpp"
struct TreeNode {
 int val;
 TreeNode *left;
 TreeNode *right;
 TreeNode() : val(0), left(nullptr), right(nullptr) {}
 TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


// @lc code=start
class Solution {
public:
    int pathSum(TreeNode* root, int k) {
        // 前缀和,先做560再做这题

        unordered_map<long long, int> m;
        m[0] = 1;
        std::function<int(TreeNode*,long long)> dfs = [&](TreeNode* root, long long sum) -> int {
            if (!root) return 0;
            sum += root->val;
            int res = 0;
            if (m.count(sum - k)) {
                res += m[sum - k];
            }
            m[sum]++;
            res += dfs(root->left, sum);
            res += dfs(root->right, sum);

            // 返回时,该子树的前缀和应该被丢弃
            m[sum]--; if (m[sum] == 0) m.erase(sum);
            return res;
        };
        return dfs(root, 0);
    }
};
// @lc code=end

