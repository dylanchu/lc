/*
 * @lc app=leetcode.cn id=647 lang=cpp
 *
 * [647] 回文子串
 *
 * https://leetcode.cn/problems/palindromic-substrings/description/
 *
 * algorithms
 * Medium (67.04%)
 * Likes:    1255
 * Dislikes: 0
 * Total Accepted:    305.3K
 * Total Submissions: 455.4K
 * Testcase Example:  '"abc"'
 *
 * 给你一个字符串 s ，请你统计并返回这个字符串中 回文子串 的数目。
 * 
 * 回文字符串 是正着读和倒过来读一样的字符串。
 * 
 * 子字符串 是字符串中的由连续字符组成的一个序列。
 * 
 * 具有不同开始位置或结束位置的子串，即使是由相同的字符组成，也会被视作不同的子串。
 * 
 * 
 * 
 * 示例 1：
 * 
 * 
 * 输入：s = "abc"
 * 输出：3
 * 解释：三个回文子串: "a", "b", "c"
 * 
 * 
 * 示例 2：
 * 
 * 
 * 输入：s = "aaa"
 * 输出：6
 * 解释：6个回文子串: "a", "a", "a", "aa", "aa", "aaa"
 * 
 * 
 * 
 * 提示：
 * 
 * 
 * 1 <= s.length <= 1000
 * s 由小写英文字母组成
 * 
 * 
 */

// @lc code=start
class Solution {
public:
    int countSubstrings(string s) {
        // 动态规划
        // dp[i][j] 表示 s[@i~@j]是否是回文子串, 只需要计数一共有多少个格子是true就行了
        // dp[i][j] = true  (长度为1)
        //          = s[i]==s[j]  (长度为2)
        //          = s[i]==s[j] && dp[i+1][j-1]
        int ans=0;
        bool dp[s.size()][s.size()];
        for (int i=s.size()-1; i>=0; --i) {
            for (int j=i; j<s.size(); ++j) {
                if (j-i == 0) {
                    dp[i][j] = true;
                    ++ans;
                }
                else if (j-i == 1) {
                    dp[i][j] = s[i] == s[j];
                    if (dp[i][j]) ++ans;
                }
                else {
                    if (i+1>=s.size() || j-1<0) {
                        dp[i][j]=false;
                        continue;
                    }
                    dp[i][j] = s[i] == s[j] && dp[i+1][j-1];
                    if (dp[i][j]) ++ans;
                }
            }
        }
        return ans;
    }
};
// @lc code=end

