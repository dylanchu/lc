// @before-stub-for-debug-begin
#include <vector>
#include <string>
#include "commoncppproblem448.h"

using namespace std;
// @before-stub-for-debug-end

/*
 * @lc app=leetcode.cn id=448 lang=cpp
 *
 * [448] 找到所有数组中消失的数字
 */

// @lc code=start
class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        vector<int> count(nums.size()+1);
        for (int n:nums) {
            count[n]++;
        }
        vector<int> res;
        for (int i=1; i<count.size(); ++i) {
            if (count[i]==0) res.push_back(i);
        }
        return res;

        // // 进阶：O(N)的时间复杂度,只占用常数额外空间
        // for (int i=0; i<nums.size(); ++i) {
        //     // nums[nums[i]-1] = 0;  可能重复取到同一位置
        //     nums[abs(nums[i])-1] = -abs(nums[abs(nums[i])-1]);
        // }
        // vector<int> res;
        // for (int i=0; i<nums.size(); ++i) {
        //     if (nums[i] > 0) {
        //         res.push_back(i+1);
        //     }
        // }
        // return res;
    }
};
// @lc code=end

