#include <vector>
#include <deque>
#include <list>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <algorithm>
#include <functional>

using namespace std;


namespace utils {
    template <typename T>
    void print_seq(T container) {
        int i = 0;
        for (auto x: container) {
            if (i >= 10) {
                std::cout << "... (" << container.size() - 10 << " more elements)" << std::endl;
                break;
            }
            std::cout << x << ", ";
        }
        std::cout << endl;
    }
}
