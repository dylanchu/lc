/*
 * @lc app=leetcode.cn id=316 lang=cpp
 *
 * [316] 去除重复字母
 *
 * https://leetcode.cn/problems/remove-duplicate-letters/description/
 *
 * algorithms
 * Medium (48.55%)
 * Likes:    1009
 * Dislikes: 0
 * Total Accepted:    126.6K
 * Total Submissions: 260.7K
 * Testcase Example:  '"bcabc"'
 *
 * 给你一个字符串 s ，请你去除字符串中重复的字母，使得每个字母只出现一次。需保证 返回结果的字典序最小（要求不能打乱其他字符的相对位置）。
 * 
 * 
 * 
 * 示例 1：
 * 
 * 
 * 输入：s = "bcabc"
 * 输出："abc"
 * 
 * 
 * 示例 2：
 * 
 * 
 * 输入：s = "cbacdcbc"
 * 输出："acdb"
 * 
 * 
 * 
 * 提示：
 * 
 * 
 * 1 <= s.length <= 10^4
 * s 由小写英文字母组成
 * 
 * 
 * 
 * 
 * 注意：该题与 1081
 * https://leetcode-cn.com/problems/smallest-subsequence-of-distinct-characters
 * 相同
 * 
 */

// @lc code=start
class Solution {
public:
    inline int index(char c) {
        return c - 'a';
    }

    string removeDuplicateLetters(string s) {
        // 单调栈
        // 而且:一旦某个字符出现且仅出现一次，那么它前面的任一字符c就不会被它后面的字符影响
        int cnt[26];
        int isin[26];
        std::memset(cnt, 0, sizeof(cnt));
        std::memset(isin, 0, sizeof(isin));
        for (auto c:s) ++cnt[index(c)];

        std::vector<char> vec;
        for (auto c:s) {
            --cnt[index(c)];
            if (isin[index(c)]) continue;

            while (!vec.empty() && vec.back()>c && cnt[index(vec.back())]>0) {
                --isin[index(vec.back())];
                vec.pop_back();
            }
            vec.push_back(c);
            ++isin[index(c)];
        }
        return string(vec.begin(), vec.end());
    }
};
// @lc code=end

