#include "../utils.hpp"

int partition1(std::vector<int>& arr, int left, int right) {
    int pivot = arr[left];
    // 左右双指针法
    while (left < right) {
        while (left < right && arr[right] >= pivot) --right;
        arr[left] = arr[right];
        while (left < right && arr[left] <= pivot) ++left;
        arr[right] = arr[left];
    }
    arr[left] = pivot;
    return left;
}

int partition2(std::vector<int>& arr, int left, int right) {
    int pivot = arr[right];
    // 快慢指针法
    // [0, slow-1]: <pivot
    // [slow, fast-1]: >=pivot
    int slow = left;
    for (int fast=left; fast<right; ++fast) {
        if (arr[fast] < pivot) {
            std::swap(arr[slow], arr[fast]);
            ++slow;
        }
    }
    std::swap(arr[slow], arr[right]);
    return slow;
}

void quick_sort(std::vector<int>& nums, int low, int high) {
    if (low < high) {
        // auto pi = partition1(nums, low, high);
        auto pi = partition2(nums, low, high);
        quick_sort(nums, low, pi-1);
        quick_sort(nums, pi+1, high);
    }
}



int main() {
    std::vector<int> nums{4,1,5,3,2};
    quick_sort(nums, 0, nums.size()-1);
    utils::print_seq(nums);

    return 0;
}