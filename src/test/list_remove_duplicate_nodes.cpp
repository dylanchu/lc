/*
 * EA笔试编程题1
 * 给定一个链表,移除链表中所有重复的节点, 如: 输入 [1,1] 输出 [].
*/


#include "../utils.hpp"
#include <vector>

struct ListNode {
    int val;
    ListNode* next=nullptr;
    ListNode(int _val) : val(_val) {}
};

// ------------------------------------------
// 链表非严格递增,要求:只要节点重复就都删除
ListNode* remove_duplicated(ListNode* head) {
    if (!head || !head->next) return head;
    if (head->val == head->next->val) {
        auto p = head->next->next;
        while (p && p->val == head->val) p = p->next;
        return remove_duplicated(p);
    }
    else {
        head->next = remove_duplicated(head->next);
        return head;
    }
}
ListNode* remove_duplicated2(ListNode* head) {
    if (!head || !head->next) return head;
    ListNode* dummy = new ListNode(head->val - 1);
    ListNode* tail = dummy;
    ListNode* pending = head;
    auto p = head->next;
    while (p && pending) {
        if (p->val == pending->val) {
            while (p && p->val == pending->val) p = p->next;
            pending = p;
            if (p) p = p->next;
        }
        else {
            tail->next = pending;
            tail = pending;
            pending = p;
            p = p->next;
        }
    }
    tail->next = pending;
    auto res = dummy->next;
    delete dummy;
    return res;
}
// ------------------------------------------

#pragma region helper-funcs
void print_list(ListNode* head) {
    int i = 0;
    std::cout << "[ ";
    while (head && ++i <= 20) {
        std::cout << head->val << (head->next ? "   " : "");
        head = head->next;
    }
    if (i > 20) std::cout << "...";
    std::cout << " ]" << std::endl;
}

void do_test(std::vector<int>& v) {
    ListNode* head=nullptr;
    ListNode* tail=nullptr;
    for (auto x:v) {
        auto p = new ListNode(x);
        if (!head) {
            head = p;
            tail = head;
        }
        tail->next = p;
        tail = p;
    }
    std::cout << " input: "; print_list(head);
    std::cout << "output: "; print_list(remove_duplicated2(head));
    std::cout << endl;
}
#pragma endregion helper-funcs

int main() {
    std::vector<std::vector<int>> testcases = {
        {},
        {1,1},
        {1,1,1,2,3},  // 2,3
        {1,2,2,3,3,4,5},  // 1,4,5
        {0,0,0,1,2,2,3,3,4,5,6,6},  // 1,4,5
    };
    for (auto& v : testcases) {
        do_test(v);
    }
    return 0;
}