/*
 * @lc app=leetcode.cn id=739 lang=cpp
 *
 * [739] 每日温度
 */

// @lc code=start
class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        std::vector<int> res(temperatures.size());
        // 对于idx来说,只要管后面第一个比它大的即可.
        // 单调栈
        // 从后往前遍历, 沉在栈底的元素不用管
        std::stack<pair<int,int>> st;  // {t,idx}
        for (int i=temperatures.size()-1; i>=0; --i) {
            while(!st.empty() && st.top().first <= temperatures[i]) {
                st.pop();
            }
            if (st.empty()) res[i] = 0;
            else res[i] = st.top().second - i;
            st.push({temperatures[i], i});
        }

        return res;
    }
};
// @lc code=end

