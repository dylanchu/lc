/*
 * @lc app=leetcode.cn id=1081 lang=cpp
 *
 * [1081] 不同字符的最小子序列
 *
 * https://leetcode.cn/problems/smallest-subsequence-of-distinct-characters/description/
 *
 * algorithms
 * Medium (58.61%)
 * Likes:    200
 * Dislikes: 0
 * Total Accepted:    28K
 * Total Submissions: 47.8K
 * Testcase Example:  '"bcabc"'
 *
 * 返回 s 字典序最小的子序列，该子序列包含 s 的所有不同字符，且只包含一次。
 * 
 * 
 * 
 * 示例 1：
 * 
 * 
 * 输入：s = "bcabc"
 * 输出："abc"
 * 
 * 
 * 示例 2：
 * 
 * 
 * 输入：s = "cbacdcbc"
 * 输出："acdb"
 * 
 * 
 * 
 * 提示：
 * 
 * 
 * 1 <= s.length <= 1000
 * s 由小写英文字母组成
 * 
 * 
 * 
 * 
 * 注意：该题与 316 https://leetcode.cn/problems/remove-duplicate-letters/ 相同
 * 
 */

// @lc code=start
class Solution {
public:
    inline int index(char c) {return c-'a';}

    string smallestSubsequence(string s) {
        // 该题和 316 相同, 单调栈
        int cnt[26];
        std::memset(cnt, 0, sizeof(cnt));
        for (char c:s) ++cnt[index(c)];

        int instack[26];
        std::memset(instack, 0, sizeof(instack));
        vector<char> vec;
        for (char c:s) {
            --cnt[index(c)];
            if (instack[index(c)]) continue;

            while (!vec.empty() && cnt[index(vec.back())] && vec.back()>c) {
                instack[index(vec.back())] = 0;
                vec.pop_back();
            }
            vec.push_back(c);
            instack[index(c)] = 1;
        }
        return string(vec.begin(), vec.end());
    }
};
// @lc code=end

